// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

interface IP2PPool {
    function getCapacity()  external view returns (uint256);
    function executePositions(address fromAccount, uint256 amount, uint80 oracleRoundId) external;
    function openPosition(uint256 amount) external returns (uint256 positionId);
    function closePosition(uint256 positionId, uint256 blockNumber) external;
}