// contracts/DtecT.sol
// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "./IDtecT.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "./IMechanics.sol";

contract Mechanics is Ownable, IMechanics {

    constructor (address admin) Ownable() {
        transferOwnership(admin);
    }

    /** Policy **/
    mapping (address => bool) private _blacklistedAccounts;
    event Blacklist(address indexed account, bool indexed blacklisted);
    function setBlacklistForAccount(address account, bool blacklisted) external onlyOwner {
        _blacklistedAccounts[account] = blacklisted;
        emit Blacklist(account, blacklisted);
    }

    function isTransferable(address token, address from, address to, uint256 amount) external override view returns (bool) {
        return !_blacklistedAccounts[from] && !_blacklistedAccounts[to];
    }

    /** Operations **/
    mapping (address => bool) private _coreContracts;
    event CoreContract(address indexed account, bool indexed enable);
    function setCoreContract(address account, bool enable) external onlyOwner {
        _coreContracts[account] = enable;
        emit CoreContract(account, enable);
    }

    function isOperable(address token, address operator, address from, address to, uint256 amount) external override view returns (bool) {
        return _coreContracts[operator];
    }

    /** Finance **/
    function mint(address token, address account, uint256 amount) external override onlyOwner {
        IDtecT(token).mint(account, amount);
    }
    function burn(address token, address account, uint256 amount) external override onlyOwner {
        IDtecT(token).burn(account, amount);
    }
}