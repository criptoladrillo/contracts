// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/metatx/ERC2771ContextUpgradeable.sol";
import "./IP2PPool.sol";
import "./IDtecT.sol";
import "./IOracle.sol";

contract P2PPool is IP2PPool, ERC2771ContextUpgradeable, OwnableUpgradeable {
    address  public  platform;
    address  public  swapContract;

    IDtecT  public takerToken;
    IDtecT  public makerToken;
    IOracle public oracle;

    uint256 public capacity;

    uint256 public commissionPercent;

    uint256 public first;
    uint256 public last;

    // Index positionId
    mapping (bytes32 => address) public owner;
    mapping (bytes32 => uint256) public takerAmount;
    mapping (bytes32 => uint256) public makerAmount;
    mapping (bytes32 => bool)    public closed;
    mapping (bytes32 => bool)    public exist;
    mapping (uint256 => uint256) public previousPosition;
    mapping (uint256 => uint256) public nextPosition;
    mapping (uint256 => uint256) public blockNumber;

    struct Position {
        address owner;
        uint256 takerAmount;
        uint256 makerAmount;
        bool    closed;
    }

    uint256 public constant COMMISSION_EXPONENT = 1000000;

    event OpenPosition(uint256 indexed positionId, bytes32 indexed positionHash);
    event ClosePosition(bytes32 indexed positionHash);
    event ExecutePosition(uint256 indexed positionId, bytes32 indexed positionHash, bool indexed completed);
    event ExecutePositions(uint256 cost, uint256 commission, uint80 indexed oracleRoundId);

    /// @custom:oz-upgrades-unsafe-allow constructor
    constructor(address forwarder_) ERC2771ContextUpgradeable(forwarder_) {
        _disableInitializers();
    }

    function initialize(address admin, address platform_, address swapContract_, IDtecT takerToken_, IDtecT makerToken_, IOracle oracle_, uint256 commissionPercent_) public initializer {
        __Ownable_init();
        transferOwnership(admin);
        platform     = platform_;
        swapContract = swapContract_;

        takerToken        = takerToken_;
        makerToken        = makerToken_;
        oracle            = oracle_;
        commissionPercent = commissionPercent_;
        capacity          = 0;

        first = 0;
        last  = 0;
    }

    function computePositionHash(uint256 positionId, uint256 blockNumber_) public pure returns(bytes32) {
        return keccak256(abi.encodePacked(positionId, blockNumber_));
    }

    function getPosition(bytes32 positionHash) public view returns(Position memory) {
        return Position(
            owner      [positionHash],
            takerAmount[positionHash],
            makerAmount[positionHash],
            closed     [positionHash]
        );
    }

    function getPosition(uint256 positionId, uint256 blockNumber_) public view returns(Position memory) {
        bytes32 positionHash = computePositionHash(positionId, blockNumber_);
        return Position(
            owner      [positionHash],
            takerAmount[positionHash],
            makerAmount[positionHash],
            closed     [positionHash]
        );
    }

    function updateCommissionPercent(uint256 commission_) public onlyOwner {
        commissionPercent = commission_;
    }

    function getCapacity() public view returns(uint256) {
        return capacity;
    }

    function executePositions(address fromAccount, uint256 amount, uint80 oracleRoundId) external override {
        require(msg.sender == swapContract, "Unable to execute");
        require(amount <= capacity, "No capacity");
        (uint80 roundID, int answer_, uint8 answerDecimals) = oracle.getLastRound();
        require(oracleRoundId == roundID, "Outdated round");
        uint256 answer = uint256(answer_);
        capacity = capacity - amount;

        uint256 currentPosition = first;

        uint256 totalNetCost;
        uint256 totalCommission;

        uint256 localNetCost;
        uint256 localCommission;
        uint256 remindMakerAmount = amount;
        bool completed;
        while (remindMakerAmount > 0){
            bytes32 positionHash = computePositionHash(currentPosition, blockNumber[currentPosition]);
            completed = makerAmount[positionHash] <= remindMakerAmount;
            if(completed) {
                localNetCost              = (makerAmount[positionHash] * answer) / 10 ** answerDecimals;
                remindMakerAmount         = remindMakerAmount - makerAmount[positionHash];
                makerAmount[positionHash] = 0;
            } else {
                localNetCost              = (remindMakerAmount * answer) / 10 ** answerDecimals;
                makerAmount[positionHash] = makerAmount[positionHash] - remindMakerAmount;
                remindMakerAmount         = 0;
            }

            localCommission = (localNetCost * commissionPercent) / COMMISSION_EXPONENT;
            takerAmount[positionHash] = takerAmount[positionHash] + (localNetCost - localCommission);
            totalCommission = totalCommission + localCommission;
            totalNetCost    = totalNetCost + localNetCost;
            emit ExecutePosition(currentPosition, positionHash, completed);
            if (completed) {
                currentPosition = nextPosition[currentPosition];
            }
        }
        if (completed){
            first = currentPosition;
            previousPosition[first] = 0;
            if (first == 0) {
                last = 0;
            }
        }
        makerToken.transfer(fromAccount, amount);
        takerToken.operateTransferFrom(fromAccount, address(this), totalNetCost - totalCommission);
        takerToken.operateTransferFrom(fromAccount, platform, totalCommission);

        emit ExecutePositions(totalNetCost, totalCommission, roundID);
    }

    function openPosition(uint256 amount) external override returns (uint256 positionId) {
        uint256 previous;

        if (first == 0) {
            first = 1;
            positionId = 1;
            previous = previousPosition[positionId] = 0;
        } else {
            positionId = last + 1;
            previous = previousPosition[positionId] = last;
        }

        // Capture funds
        makerToken.operateTransferFrom(_msgSender(), address(this), amount);
        // Add capacity
        capacity = capacity + amount;
        // Set position values
        bytes32 positionHash = computePositionHash(positionId, block.number);
        owner            [positionHash] = _msgSender();
        takerAmount      [positionHash] = 0;
        makerAmount      [positionHash] = amount;
        closed           [positionHash] = false;
        exist            [positionHash] = true;
        nextPosition     [positionId]   = 0;
        blockNumber      [positionId]   = block.number;

        if (previous != 0){
            nextPosition[previous] = positionId;
        }

        last = positionId;

        emit OpenPosition(positionId, positionHash);
        return positionId;
    }

    function closePosition(uint256 positionId, uint256 blockNumber_) external override {
        bytes32 positionHash = computePositionHash(positionId, blockNumber_);
        require(exist   [positionHash],   "Not exist");
        require(!closed [positionHash], "Already closed");

        closed[positionHash] = true;

        if (makerAmount[positionHash] > 0) {
            require(owner[positionHash] == _msgSender(), "Not delegable");
            makerToken.transfer(owner[positionHash],   makerAmount[positionHash]);
            capacity = capacity - makerAmount[positionHash];
        }
        if (takerAmount[positionHash] > 0) {
            takerToken.transfer(owner[positionHash], takerAmount[positionHash]);
        }

        if (blockNumber[positionId] == blockNumber_){
            if (first == positionId || last  == positionId) {
                if (first == positionId) {
                    first = nextPosition[positionId];
                    previousPosition[first] = 0;
                }
                if (last == positionId) {
                    last = previousPosition[positionId];
                    nextPosition[last] = 0;
                }
            } else {
                nextPosition[previousPosition[positionId]] = nextPosition[positionId];
                previousPosition[nextPosition[positionId]] = previousPosition[positionId];
            }
        }

        emit ClosePosition(positionHash);
    }

    function _msgSender() internal view virtual override(ContextUpgradeable, ERC2771ContextUpgradeable) returns (address) {
        return ERC2771ContextUpgradeable._msgSender();
    }

    function _msgData() internal view virtual override(ContextUpgradeable, ERC2771ContextUpgradeable) returns (bytes calldata) {
        return ERC2771ContextUpgradeable._msgData();
    }

    function setPlatform(address platform_) external onlyOwner {
        platform = platform_;
    }

    function setOracle(IOracle oracle_) external onlyOwner {
        oracle = oracle_;
    }

    function setSwapContract(address swapContract_) external onlyOwner {
        swapContract = swapContract_;
    }
}