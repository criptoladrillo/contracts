import { expect } from 'chai'
import { ethers } from 'hardhat'
import {Forwarder, Mechanics, DtecT} from "../typechain";
import {SignerWithAddress} from "@nomiclabs/hardhat-ethers/dist/src/signer-with-address";
import {EIP712Domain, ForwardRequest, shouldFailWith, singMetatx} from "./utils";

interface TestContext {
    signers: {
        [id: string]: SignerWithAddress
    }
    token: DtecT
    mechanics: Mechanics
    forwarder: Forwarder
}

describe('Forwarder', function() {

    let Context: TestContext

    before(async function () {
        const [admin, service, user1, user2, user3, thirdparty] = await ethers.getSigners()

        const Forwarder = await ethers.getContractFactory("Forwarder")
        const forwarder = await Forwarder.deploy(admin.address,service.address,'name','version') as Forwarder;

        const Mechanics = await ethers.getContractFactory("Mechanics")
        const mechanics = await Mechanics.deploy(admin.address) as Mechanics;

        const Token = await ethers.getContractFactory("DtecT")
        const token = await Token.deploy(admin.address, mechanics.address, forwarder.address, "A", "A") as DtecT;

        await mechanics.connect(admin).mint(token.address, user1.address, ethers.utils.parseUnits('10'))
        await mechanics.connect(admin).mint(token.address, user3.address, ethers.utils.parseUnits('10'))

        Context = {
            forwarder,
            token,
            mechanics,
            signers: {
                admin,
                service,
                user1,
                user2,
                user3,
                thirdparty,
            }
        }
    })

    describe("execute", async () => {
        let local: any
        before(async () => {
            const {token, forwarder, signers: {admin, user1, user2}} = Context
            const data = token.interface.encodeFunctionData("transfer", [
                user2.address,
                ethers.utils.parseUnits('1')
            ])
            const network = await ethers.provider.getNetwork()
            const gas = await ethers.provider.estimateGas({
                from: user1.address,
                to: token.address,
                data
            })
            const domain: EIP712Domain = {
                chainId: network.chainId,
                name: 'name',
                version: 'version',
                verifyingContract: forwarder.address
            }
            const message: ForwardRequest = {
                from: user1.address,
                to: token.address,
                value: 0,
                nonce: 0,
                gas: gas.toNumber(),
                data
            }
            const signature = await singMetatx(user1.address, domain, message)
            local = {
                domain,
                message,
                signature
            }
        })


        it("call from thirdparty should fail", async()=>{
            const {forwarder, signers: {thirdparty}} = Context
            await expect(
                forwarder.connect(thirdparty).execute(local.message,local.signature)
            ).to.revertedWith('onlyForwarder')
        })

        it("call from admin should success", async()=>{
            const {token, forwarder, signers: {service, user1, user2}} = Context
            const tx = await forwarder.connect(service).execute(local.message,local.signature)
            await ethers.provider.send('evm_mine', [])
            const receipt = await tx.wait(1)
            const filter = token.filters.Transfer(user1.address, user2.address, undefined)
            const events = await token.queryFilter(filter)
            expect(events.length).eq(1)
            const event = events.pop()
            expect(event?.blockNumber).eq(receipt.blockNumber)
        })

        it('account nonce should be updated', async () => {
            const {forwarder, signers: {user1}} = Context
            const nonce = await forwarder.getNonce(user1.address)
            expect(nonce.toNumber()).eq(1)
        });

        xit('should emit an operation hash', async () => {
            /*
            const {forwarder, signers: {user1}} = Context
            const filter = forwarder.filters.Operation(ethers.utils.solidityKeccak256([ "string" ], [ 'OperationId' ]))
            const events = await forwarder.queryFilter(filter)
            expect(events.length).eq(1)
             */
        });

        it("invalid nonce should fails", async () => {
            const {forwarder, signers: {service}} = Context
            await expect(
                forwarder.connect(service).execute(local.message,local.signature)
            ).to.revertedWith('Invalid signature')
        })

        it('should update forwarder works', async () => {
            const {token, signers: {admin, service, user1, user2, user3}} = Context

            const Forwarder = await ethers.getContractFactory("Forwarder")
            const forwarder = await Forwarder.deploy(admin.address,service.address,'name','version') as Forwarder;

            const data = token.interface.encodeFunctionData("transfer", [
                user2.address,
                ethers.utils.parseUnits('1')
            ])

            const network = await ethers.provider.getNetwork()
            const gas = await ethers.provider.estimateGas({
                from: user3.address,
                to: token.address,
                data
            })
            const domain: EIP712Domain = {
                chainId: network.chainId,
                name: 'name',
                version: 'version',
                verifyingContract: forwarder.address
            }
            const message: ForwardRequest = {
                from: user3.address,
                to: token.address,
                value: 0,
                nonce: 0,
                gas: gas.toNumber(),
                data
            }
            const signature = await singMetatx(user3.address, domain, message)

            await token.connect(admin).updateForwarder(forwarder.address)

            const tx = await forwarder.connect(service).execute(message,signature)
            await ethers.provider.send('evm_mine', [])
            const receipt = await tx.wait(1)
            const filter = token.filters.Transfer(user3.address, user2.address, undefined)
            const events = await token.queryFilter(filter)
            expect(events.length).eq(1)
            const event = events.pop()
            expect(event?.blockNumber).eq(receipt.blockNumber)
        });
    })
})