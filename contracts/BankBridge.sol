// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/metatx/ERC2771Context.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/access/AccessControl.sol";
import "@openzeppelin/contracts/security/Pausable.sol";
import "./IDtecT.sol";

contract BankBridge is Ownable, AccessControl, ERC2771Context, Pausable {

    bytes32 public constant TAPIR_ROLE = keccak256("TAPIR_ROLE");
    mapping (bytes32 => bool) public _knowOperations;
    address private _forwarder;

    constructor( address admin, address forwarder, address tapir, bytes32[] memory operationHashes) ERC2771Context(forwarder) Ownable() Pausable() {
        _forwarder = forwarder;
        transferOwnership(admin);
        _setupRole(DEFAULT_ADMIN_ROLE, admin);
        _setupRole(TAPIR_ROLE, tapir);
        for (uint256 index; index < operationHashes.length; index++) {
            _knowOperations[operationHashes[index]] = true;
        }
    }

    function isTrustedForwarder(address forwarder)
        public
        view
        override
        returns (bool)
    {
        return forwarder == _forwarder;
    }

    function updateForwarder(address forwarder) external onlyOwner {
        _forwarder = forwarder;
    }


    /**
        @notice marks an operation as known, so that it does not repeat the same one, carpincho verifies before a withdrawal that the operation is not known.
        @dev adds to the mapping a new operation hash
     */
    function knowOperation(bytes32 operationHash) public view returns (bool) {
        return _knowOperations[operationHash];
    }


    /**
        @notice Mark a single operation as knowed
        @dev set the value pair for the _knowOperations mapping 
     */
    function markOperationAsKnowed(bytes32 operationHash) public onlyOwner {
        _knowOperations[operationHash] = true;
    }

    /**
        @notice Mark a collection of operations as knowed
        @dev set the value pair for the _knowOperations mapping 
        @param operationHashes collection of transaction hashes provided by Carpincho microservice
     */
    function markOperationsAsKnowed(bytes32[] memory operationHashes)
        public
        onlyOwner
    {
        for (uint256 index; index < operationHashes.length; index++) {
            _knowOperations[operationHashes[index]] = true;
        }
    }



    /**
        @notice Only the auth account (owner) can `Pause` the contract
        @dev pause all the fn that use the modifiers inherited from Pausable()
     */
    function pause() external onlyOwner {
        _pause();
    }


    /**
        @notice Unpause the contract
     */
    function unpause() external onlyOwner {
        _unpause();
    }

    event Deposit(
        bytes32 indexed operationHash,
        address indexed destination,
        uint256 amount,
        IDtecT indexed token
    );


    /**
        @notice Creates a single deposit 
        @dev verify that the hash is not already saved if not, save the hash and make the deposit
        @param operationHash generated from `Carpincho`
        @param destination wallet of the user that make the deposit 
        @param token tokenized repensentation of ARS ( more in the future? DAI!!)
     */
    function _deposit(
        bytes32 operationHash,
        address destination,
        uint256 amount,
        IDtecT token
    ) internal {
        if (_knowOperations[operationHash]) return;
        _knowOperations[operationHash] = true;

        token.transfer(destination, amount);
        emit Deposit(operationHash, destination, amount, token);
    }

    function deposit(
        bytes32 operationHash,
        address destination,
        uint256 amount,
        IDtecT token
    ) public onlyRole(TAPIR_ROLE) whenNotPaused {
        _deposit(operationHash, destination, amount, token);
    }


    /**
        @notice  Batch version of deposit, receives same params but of type array[]
     */
    function batchDeposit(
        bytes32[] memory operationHashes,
        address[] memory destinations,
        uint256[] memory amounts,
        IDtecT token
    ) external onlyRole(TAPIR_ROLE) whenNotPaused {
        require(
            operationHashes.length == amounts.length,
            "Wrong params length"
        );
        require(amounts.length == destinations.length, "Wrong params length");
        for (uint256 index; index < operationHashes.length; index++) {
            _deposit(
                operationHashes[index],
                destinations[index],
                amounts[index],
                token
            );
        }
    }

    event Withdrawal(
        bytes32 indexed operationHash,
        address indexed origin,
        uint256 amount,
        IERC20 indexed token
    );

    /**
        @notice Creates a single withdrawal 
        @dev verify that the hash is not already saved, gets the origin from the sender
        @param operationHash generated from `Carpincho`
        @param amount tokens from origin to this contract
     */
    function withdrawal(
        bytes32 operationHash,
        uint256 amount,
        IDtecT token
    ) external whenNotPaused {
        require(!_knowOperations[operationHash], "Known operation");
        _knowOperations[operationHash] = true;

        address origin = _msgSender();
        token.operateTransferFrom(origin, address(this), amount);

        emit Withdrawal(operationHash, origin, amount, token);
    }

    function _msgSender()
        internal
        view
        virtual
        override(Context, ERC2771Context)
        returns (address)
    {
        return ERC2771Context._msgSender();
    }

    function _msgData()
        internal
        view
        virtual
        override(Context, ERC2771Context)
        returns (bytes calldata)
    {
        return ERC2771Context._msgData();
    }
}
