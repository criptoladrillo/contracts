// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

interface IOracle {
    function getLastRound() external view returns (uint80 roundId, int256 answer, uint8 decimals);
}