import hre, {upgrades, ethers as hrtEthers, network} from "hardhat"
import {ContractFactory, ethers, PopulatedTransaction, Signer} from "ethers"
import {
    DtecT,
    DtecT__factory,
    Mechanics,
    Mechanics__factory,
    Forwarder,
    Forwarder__factory,
    P2PPool,
    SwapWithOracles,
    Oracle__factory, BankBridge__factory, BankBridge, Oracle,
} from "../typechain"
import EthersAdapter from '@gnosis.pm/safe-ethers-lib'
import Safe, {SafeFactory, SafeAccountConfig} from '@gnosis.pm/safe-core-sdk'
import {parseEther} from "@ethersproject/units";

const defaultNetworkAddress={
    safeProxyFactoryAddress:  '0xa6B71E26C5e0845f74c812102Ca7114b6a896AB2',
    safeMasterCopyAddress:    '0x3E5c63644E683549055b9Be8653de26E0B4CD36E',
    multiSendCallOnlyAddress: '0x40A2aCCbd92BCA938b02010E17A5b8929b49130D',
    multiSendAddress:         '0xA238CBeb142c10Ef7Ad8442C6D1f9E89e07e7761'
}
const contractNetworks={
    31337: {
        safeProxyFactoryAddress:  '0xE89ce3bcD35bA068A9F9d906896D3d03Ad5C30EC',
        safeMasterCopyAddress:    '0xE51abdf814f8854941b9Fe8e3A4F65CAB4e7A4a8',
        multiSendCallOnlyAddress: '0x40A2aCCbd92BCA938b02010E17A5b8929b49130D',
        multiSendAddress:         '0xA238CBeb142c10Ef7Ad8442C6D1f9E89e07e7761'
    },
    5: defaultNetworkAddress,
    100: defaultNetworkAddress,
}


const carpinchoAddress          = process.env.CARPINCHO_ADDRESS  || "0xAd1ec246a406A95162aB733774cD5590766A4812"
const carpinchoSecondaryAddress = process.env.CARPINCHO2_ADDRESS || "0xdA3599D3354F69B411aEC286f8324fEeD1cE0099"
const tapirAddress              = process.env.TAPIR_ADDRESS      || "0x9127566983B4B9B7fc051dbC713C6e52B565c923"
const verifySourceCode          = process.env.VERIFY_SOURCE_CODE === "1"

hrtEthers.provider.getNetwork().then(async ({chainId}) => {
    let signer: Signer
    if (chainId === 31337) {
        [signer] = await hrtEthers.getSigners()
    } else {
        signer = new ethers.Wallet(process.env.PK as string, hrtEthers.provider)
    }
    console.log('wallet', signer.getAddress())
    await deploy(signer, carpinchoAddress, tapirAddress)
    await signer.sendTransaction({
        to: carpinchoAddress,
        value: ethers.utils.parseEther('0.5')
    })
    await signer.sendTransaction({
        to: carpinchoSecondaryAddress,
        value: ethers.utils.parseEther('0.5')
    })
    await signer.sendTransaction({
        to: tapirAddress,
        value: ethers.utils.parseEther('0.5')
    })
})

export async function deployMultisign(wallet: Signer){
    const ethAdapter = new EthersAdapter({
        ethers,
        signer: wallet
    })
    console.log("network", await wallet.provider?.getNetwork())
    // @ts-ignore
    const safeFactory = await SafeFactory.create({ ethAdapter, contractNetworks })
    const owners = [await wallet.getAddress()]
    const threshold = 1
    const safeAccountConfig: SafeAccountConfig = {
        owners,
        threshold,
    }

    let saltNonce = '0'
    const admin=           await safeFactory.deploySafe({
        safeAccountConfig,
        safeDeploymentConfig: { saltNonce }
    })
    console.log('multisign admin', admin.getAddress(), saltNonce)

    saltNonce = '1'
    const fee= await safeFactory.deploySafe({
        safeAccountConfig,
        safeDeploymentConfig: { saltNonce }
    })
    console.log('multisign fee', fee.getAddress(), saltNonce)

    saltNonce = '2'
    const liquityProvider= await safeFactory.deploySafe({
        safeAccountConfig,
        safeDeploymentConfig: { saltNonce }
    })
    console.log('multisign liquityProvider', liquityProvider.getAddress(), saltNonce)


    return {
        admin,
        fee,
        liquityProvider
    }
}

export async function deployContract(factory: ContractFactory, params: any[], verifyCode=verifySourceCode){
    const instance = await factory.deploy(...params)
    await instance.deployTransaction.wait(1)
    if (verifyCode) {
        await hre.run("verify", {
            address: instance.address,
            constructorArgs: params
        })
    }
    return instance
}

export async function deployForwarder(wallet: Signer, adminAddress: string, carpinchoAddress: string, name="Criptoladrillo", version="2"){
    const forwarder = await deployContract(new Forwarder__factory(wallet),[adminAddress, carpinchoAddress, name, version])
    console.log('forwarder', forwarder.address, {carpinchoAddress})
    return forwarder as Forwarder
}

export async function deployMechanics(wallet: Signer, adminAddress: string){
    const mechanics = await deployContract(new Mechanics__factory(wallet), [adminAddress])
    console.log('mechanics', mechanics.address)
    return mechanics as Mechanics
}

export async function deployBankBridge(wallet: Signer, TapirAddress: string, forwarderAddress: string, adminAddress: string, tapirAddress: string, knowOperationHashes = [ethers.constants.HashZero]){
    const bankBridge = await deployContract(new BankBridge__factory(wallet), [adminAddress, forwarderAddress, tapirAddress, knowOperationHashes])
    console.log('bankBridge', bankBridge.address)
    return bankBridge as BankBridge
}

export async function deployOracle(wallet: Signer, adminAddress: string, initialPrice = 56, decimals = 6) {
    const price = ethers.utils.parseUnits(initialPrice.toString(), decimals)
    const oracle = await deployContract(new Oracle__factory(wallet), [adminAddress, price])
    console.log('oracle', oracle.address)
    return oracle as Oracle
}

export async function deployP2P(wallet: Signer, adminAddress: string, forwarderAddress: string, swapAddress: string, feeCollectorAddress: string, tokenARSDAddress: string, tokenCLADAddress: string, oracleAddress: string, initialComission = 0.02) {
    const comission = ethers.utils.parseUnits(initialComission.toString(), 6)
    const P2PPool = await hrtEthers.getContractFactory("P2PPool", wallet)
    const p2PPool = await upgrades.deployProxy(P2PPool, [adminAddress, feeCollectorAddress, swapAddress, tokenARSDAddress, tokenCLADAddress, oracleAddress, comission], {
        constructorArgs: [forwarderAddress]
    }) as P2PPool
    await p2PPool.deployTransaction.wait(1)
    console.log('p2PPool', p2PPool.address)
    return p2PPool
}

export async function deploySwap(wallet: Signer, adminAddress: string, forwarderAddress: string, feeCollectorAddress: string){
    const SwapWithOracles = await hrtEthers.getContractFactory("SwapWithOracles", wallet)
    const swap = await upgrades.deployProxy(SwapWithOracles, [adminAddress, feeCollectorAddress], {
        constructorArgs: [forwarderAddress]
    }) as SwapWithOracles
    await swap.deployTransaction.wait(1)
    console.log('swap', swap.address)
    return swap
}

export async function deployToken(wallet: Signer, adminAddress: string, forwarderAddress: string, mechanicsAddress: string, name = "Peso Argentino", symbol = "ARS") {
    const token = await deployContract(new DtecT__factory(wallet), [adminAddress, mechanicsAddress, forwarderAddress, name, symbol])
    await token.deployTransaction.wait(1)
    console.log("token", symbol, token.address)
    return token as DtecT
}

async function deployTokens(wallet: Signer, adminAddress: string, forwarderAddress: string, mechanicsAddress: string) {
    return {
        ARSD: await deployToken(wallet, adminAddress, forwarderAddress, mechanicsAddress, "Peso Argentino", "ARS"),
        CLAD: await deployToken(wallet, adminAddress, forwarderAddress, mechanicsAddress, "Criptoladrillo", "CLAD")
    }
}

async function executeMultisigTransaction(multisig: Safe, transaction: PopulatedTransaction){
    const safeTransaction = await multisig.createTransaction({
        safeTransactionData: {
            to: transaction.to as string,
            value: '0',
            data: transaction.data as string,
        }
    })
    await multisig.executeTransaction(safeTransaction)
}

async function addPair(swap: SwapWithOracles, admin: Safe, fromTokenAddress: string, toTokenAdress: string, p2pAddress: string, oracleAddress: string, liquityAddress: string, commission: number){
    const addPair = await swap.populateTransaction.addPair({
        oracle: oracleAddress,
        commission: ethers.utils.parseUnits(commission.toString(), 6),
        p2p: p2pAddress,
        fromToken: fromTokenAddress,
        toToken: toTokenAdress,
        provider: liquityAddress
    })
    await executeMultisigTransaction(admin, addPair)
}

async function setCoreContract(admin: Safe, mechanics: Mechanics, constractAddress: string){
    const setCoreContract = await mechanics.populateTransaction.setCoreContract(constractAddress, true)
    await executeMultisigTransaction(admin, setCoreContract)
}

async function setCoreContracts(admin: Safe, mechanics: Mechanics, swapAddress: string, p2pAddress: string, bankBridgeAddress: string){
    await setCoreContract(admin, mechanics, swapAddress)
    await setCoreContract(admin, mechanics, p2pAddress)
    await setCoreContract(admin, mechanics, bankBridgeAddress)
}

async function provideLiquidity(admin: Safe, mechanics: Mechanics, tokensARSDAddress: string, tokenCLADAddress: string, bankBridgeAddress: string, liquidityProviderAddress: string, arsAmount: number, cladAmount: number){
    const arsAmountUint = ethers.utils.parseUnits(arsAmount.toString())
    const cladAmountUint = ethers.utils.parseUnits(cladAmount.toString())
    const mintArs = await mechanics.populateTransaction.mint(tokensARSDAddress, bankBridgeAddress, arsAmountUint)
    const mintClad = await mechanics.populateTransaction.mint(tokenCLADAddress, liquidityProviderAddress, cladAmountUint)
    await executeMultisigTransaction(admin, mintArs)
    console.log("Minted ARS", bankBridgeAddress, arsAmount)
    await executeMultisigTransaction(admin, mintClad)
    console.log("Minted CLAD", liquidityProviderAddress, cladAmount)
}

async function deploy(wallet: Signer, carpinchoAddress: string, tapirAddress: string, price=56, commission=0.02, cladAmount= 1000, arsAmount= 57120) {
    const multisign  = await deployMultisign(wallet)

    const adminAddress   = multisign.admin.getAddress()
    const feeAddress     = multisign.fee.getAddress()
    const liquityProviderAddress = multisign.liquityProvider.getAddress()

    const forwarder  = await deployForwarder(wallet, adminAddress, carpinchoAddress)
    const mechanics  = await deployMechanics(wallet, adminAddress)
    const tokens     = await deployTokens(wallet, adminAddress, forwarder.address, mechanics.address)
    const swap       = await deploySwap(wallet, adminAddress,forwarder.address,feeAddress)
    const bankBridge = await deployBankBridge(wallet, tapirAddress, forwarder.address, adminAddress, tapirAddress)
    const oracle     = await deployOracle(wallet, adminAddress, price)
    const p2pMarket  = await deployP2P(wallet, adminAddress, forwarder.address, swap.address, feeAddress, tokens.ARSD.address, tokens.CLAD.address, oracle.address, commission)

    await setCoreContracts(multisign.admin, mechanics, swap.address, p2pMarket.address, bankBridge.address)
    await addPair(swap, multisign.admin, tokens.ARSD.address, tokens.CLAD.address, p2pMarket.address, oracle.address, liquityProviderAddress, commission)

    await provideLiquidity(multisign.admin, mechanics, tokens.ARSD.address, tokens.CLAD.address, bankBridge.address, liquityProviderAddress, arsAmount, cladAmount)

    const approveClads = await tokens.CLAD.populateTransaction.approve(swap.address, parseEther(cladAmount.toString()))
    await executeMultisigTransaction(multisign.liquityProvider, approveClads)
    console.log("Dispose CLAD", swap.address, cladAmount)
}