import { ethers, time, upgrades } from 'hardhat'
import {Forwarder, SwapWithOracles, Oracle, Mechanics, DtecT, P2PPool} from "../typechain";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/dist/src/signer-with-address";
import { expect } from "chai";
import {afterEach, before} from "mocha";
import {comparePosition, EIP712Domain, ForwardRequest, singMetatx} from './utils';
import Decimal from 'decimal.js';
import {BigNumber} from "ethers";

interface TestContext {
    signers: {
        [id: string]: SignerWithAddress
    }
    tokenARS: DtecT
    tokenCLAD: DtecT
    pairHash: string
    mechanics: Mechanics
    oracle: Oracle
    swap: SwapWithOracles,
    p2PPool: P2PPool
    forwarder: Forwarder
}

describe('Swap', function() {

    let Context: TestContext

    before(async function () {
        const [admin, service, provider ,user, user2, vault, vault2, thirdPart] = await ethers.getSigners()

        const Forwarder = await ethers.getContractFactory("Forwarder")
        const forwarder = await Forwarder.deploy(admin.address, service.address, 'name', 'version') as Forwarder;

        const Mechanics = await ethers.getContractFactory("Mechanics")
        const mechanics = await Mechanics.deploy(admin.address) as Mechanics;

        const Token = await ethers.getContractFactory("DtecT")
        const tokenARS = await Token.deploy(admin.address, mechanics.address, forwarder.address, "A", "A") as DtecT;
        const tokenCLAD = await Token.deploy(admin.address, mechanics.address, forwarder.address, "B", "B") as DtecT;

        const Oracle = await ethers.getContractFactory("Oracle")
        const oracle = await Oracle.deploy(admin.address, ethers.utils.parseUnits('70',6)) as Oracle

        const Swap = await ethers.getContractFactory("SwapWithOracles")
        const swap = await upgrades.deployProxy(Swap, [admin.address, vault.address], {
            constructorArgs: [forwarder.address]
        }) as SwapWithOracles

        const P2PPool = await ethers.getContractFactory("P2PPool")
        const p2PPool = await upgrades.deployProxy(P2PPool, [admin.address, vault2.address, swap.address, tokenARS.address, tokenCLAD.address, oracle.address, 20000], {
            constructorArgs: [forwarder.address]
        }) as P2PPool

        const mechanicsConnected = mechanics.connect(admin)
        await mechanicsConnected.setCoreContract(swap.address, true)
        // 140 A
        await mechanicsConnected.mint(tokenARS.address, user.address, ethers.utils.parseUnits('140'))
        // 2   B
        await mechanicsConnected.mint(tokenCLAD.address, provider.address, ethers.utils.parseUnits('2'))
        await tokenCLAD.connect(provider).approve(swap.address, ethers.utils.parseUnits('1'))

        const pairHash = ethers.utils.solidityKeccak256(['address', 'address'], [tokenARS.address, tokenCLAD.address])

        Context = {
            tokenARS,
            tokenCLAD,
            pairHash,
            mechanics,
            swap,
            oracle,
            p2PPool,
            forwarder,
            signers: {
                admin,
                service,
                provider,
                user,
                user2,
                vault,
                vault2,
                thirdPart,
            }
        }
    })
    describe('addPair', () => {
        it('Thirdparty should fail', async () => {
            const {swap, tokenARS, tokenCLAD, oracle, p2PPool, signers: {thirdPart, provider}} = Context
            await expect(
                swap.connect(thirdPart)
                .addPair({
                    fromToken: tokenARS.address,
                    toToken: tokenCLAD.address,
                    oracle: oracle.address,
                    provider: provider.address,
                    p2p: p2PPool.address,
                    commission: 20000
                })
            ).to.revertedWith('owner')
        })
        it('Owner should succeed', async () => {
            const {swap, tokenARS, tokenCLAD, oracle, p2PPool, signers: {admin, provider}} = Context
            const tx = await swap.connect(admin)
                .addPair({
                    fromToken:tokenARS.address,
                    toToken: tokenCLAD.address,
                    oracle: oracle.address,
                    provider: provider.address,
                    p2p: p2PPool.address,
                    commission: 20000
                })
            const receipt = await tx.wait(1)
            expect(receipt.status).eq(1)
        })
        it('Get pair data', async () => {
            const {pairHash, swap, oracle, signers: {provider}} = Context
            const pair = await swap["getPair(bytes32)"](pairHash)
            expect(pair.provider).eq(provider.address)
            expect(pair.oracle).eq(oracle.address)
            expect(pair.commission.toString()).eq(ethers.utils.parseUnits('0.02', 6).toString())
        })
    })
    xdescribe('setVault', () => {
        it('Thirdparty should fail', async () => {
            await expect(
                Context.swap.connect(Context.signers.thirdPart).setPlatform(Context.signers.vault.address)
            ).revertedWith("Ownable: caller is not the owner")
        })
        it('Owner should succeed', async () => {
            const tx = Context.swap.connect(Context.signers.admin).setPlatform(Context.signers.vault.address)
            await tx
        })
    })
    xdescribe('swap', () => {
        const fromAmount = ethers.utils.parseUnits('70')
        it('Direct invalid round', async () => {
            await expect(
                Context.swap.connect(Context.signers.user)
                .swap(Context.pairHash, fromAmount, 0, ethers.constants.HashZero)
            ).to.revertedWith('Outdated round')
        })
        it('Direct valid round', async () => {
            let fromBalance = await Context.tokenARS.balanceOf(Context.signers.user.address)
            expect(fromBalance.toString()).eq(ethers.utils.parseUnits('140').toString())
            let vaultBalance = await Context.tokenARS.balanceOf(Context.signers.vault.address)
            expect(vaultBalance.toString()).eq('0')
            let providerBalance = await Context.tokenARS.balanceOf(Context.signers.provider.address)
            expect(providerBalance.toString()).eq('0')
            let providerBBalance = await Context.tokenCLAD.balanceOf(Context.signers.provider.address)
            expect(providerBBalance.toString()).eq(ethers.utils.parseUnits('2').toString())

            await Context.swap.connect(Context.signers.user).swap(Context.pairHash, fromAmount, 1, ethers.constants.HashZero)

            fromBalance = await Context.tokenARS.balanceOf(Context.signers.user.address)
            expect(fromBalance.toString()).eq(ethers.utils.parseUnits('68.6').toString())
            vaultBalance = await Context.tokenARS.balanceOf(Context.signers.vault.address)
            expect(vaultBalance.toString()).eq((ethers.utils.parseUnits('1.4')).toString())
            providerBalance = await Context.tokenARS.balanceOf(Context.signers.provider.address)
            expect(providerBalance.toString()).eq(ethers.utils.parseUnits('70').toString())
        })
    })
    describe('P2P', () => {
        let positionHash: string[] = []
        let blockNumber:  number[] = []
        before(async () => {
            await Context.mechanics.connect(Context.signers.admin).mint(Context.tokenARS.address, Context.signers.user.address, ethers.utils.parseUnits('100'))
            await Context.mechanics.connect(Context.signers.admin).mint(Context.tokenCLAD.address, Context.signers.user2.address, ethers.utils.parseUnits('100'))
            await Context.mechanics.setCoreContract(Context.p2PPool.address, true)
        })
        describe('Manage positions', () => {
            it('first position should be move balance', async () => {
                await expect(
                    async () => {
                        const receipt = await Context.p2PPool.connect(Context.signers.user2).openPosition(ethers.utils.parseUnits('3'))
                        positionHash[1] = ethers.utils.solidityKeccak256(['uint256', 'uint256'], [1, receipt.blockNumber])
                        blockNumber [1] = receipt.blockNumber as number
                    }
                ).to.changeTokenBalances(Context.tokenCLAD, [
                    Context.signers.user2,
                    Context.p2PPool
                ], [
                    ethers.utils.parseUnits('-3'),
                    ethers.utils.parseUnits('3'),
                ]);
            })
            it('check first position contract status', async () => {
                expect(await Context.p2PPool.first()).eq('1')
                expect(await Context.p2PPool.last()).eq('1')
                const position = await Context.p2PPool["getPosition(bytes32)"](positionHash[1])

                expect(position.closed).eq(false)
                expect(position.owner).eq(Context.signers.user2.address)
                expect(position.takerAmount).eq('0')
                expect(position.makerAmount).eq(ethers.utils.parseUnits('3'))

                expect(await Context.p2PPool.nextPosition(1)).eq(0)
                expect(await Context.p2PPool.previousPosition(1)).eq(0)
            })
            it("second position context should emit OpenPosition event with operationId == 2",async () => {
                const tx = Context.p2PPool.connect(Context.signers.user2).openPosition(ethers.utils.parseUnits('5'))
                const receipt = await tx
                positionHash[2] = ethers.utils.solidityKeccak256(['uint256', 'uint256'], [2, receipt.blockNumber])
                blockNumber [2] = receipt.blockNumber as number
                await expect(tx).to.emit(Context.p2PPool, 'OpenPosition').withArgs('2', positionHash[2])
                expect(await Context.p2PPool.first(), 'first').eq('1')
                expect(await Context.p2PPool.last(), 'last').eq('2')
                const position1 = await Context.p2PPool["getPosition(bytes32)"](positionHash[1])
                const position2 = await Context.p2PPool["getPosition(bytes32)"](positionHash[2])
                comparePosition(position1, {
                    closed: false,
                    takerAmount: ethers.utils.parseUnits('0'),
                    makerAmount: ethers.utils.parseUnits('3'),
                    owner: Context.signers.user2.address
                })
                expect(await Context.p2PPool.nextPosition(1)).eq(2)
                expect(await Context.p2PPool.previousPosition(1)).eq(0)
                comparePosition(position2, {
                    closed: false,
                    takerAmount: ethers.utils.parseUnits('0'),
                    makerAmount: ethers.utils.parseUnits('5'),
                    owner: Context.signers.user2.address,
                })
                expect(await Context.p2PPool.nextPosition(2)).eq(0)
                expect(await Context.p2PPool.previousPosition(2)).eq(1)
            })
            it("third position",async () => {
                const tx = Context.p2PPool.connect(Context.signers.user2).openPosition(ethers.utils.parseUnits('1'))
                const receipt = await tx;
                positionHash[3] = ethers.utils.solidityKeccak256(['uint256', 'uint256'], ['3', receipt.blockNumber])
                blockNumber [3] = receipt.blockNumber as number
                await expect(tx).to.emit(Context.p2PPool, 'OpenPosition').withArgs('3', positionHash[3])
                expect(await Context.p2PPool.first()).eq('1')
                expect(await Context.p2PPool.last()).eq('3')
                const position1 = await Context.p2PPool["getPosition(bytes32)"](positionHash[1])
                const position2 = await Context.p2PPool["getPosition(bytes32)"](positionHash[2])
                const position3 = await Context.p2PPool["getPosition(bytes32)"](positionHash[3])
                comparePosition(position1, {
                    closed: false,
                    takerAmount: ethers.utils.parseUnits('0'),
                    makerAmount: ethers.utils.parseUnits('3'),
                    owner: Context.signers.user2.address,
                })
                expect(await Context.p2PPool.nextPosition(1)).eq(2)
                expect(await Context.p2PPool.previousPosition(1)).eq(0)
                comparePosition(position2, {
                    closed: false,
                    takerAmount: ethers.utils.parseUnits('0'),
                    makerAmount: ethers.utils.parseUnits('5'),
                    owner: Context.signers.user2.address,
                })
                expect(await Context.p2PPool.nextPosition(2)).eq(3)
                expect(await Context.p2PPool.previousPosition(2)).eq(1)
                comparePosition(position3, {
                    closed: false,
                    takerAmount: ethers.utils.parseUnits('0'),
                    makerAmount: ethers.utils.parseUnits('1'),
                    owner: Context.signers.user2.address,
                })
                expect(await Context.p2PPool.nextPosition(3)).eq(0)
                expect(await Context.p2PPool.previousPosition(3)).eq(2)
            })
            it('Updated capacity', async () => {
                expect(
                    await Context.p2PPool.getCapacity()
                ).eq(ethers.utils.parseUnits('9'))
                expect(
                    await Context.tokenCLAD.balanceOf(Context.p2PPool.address)
                ).eq(ethers.utils.parseUnits('9'))
            })
            describe('Cancel orders', () => {
                let snapshot:string
                beforeEach(async () => {
                    snapshot = await time.snapshot()
                })
                afterEach(async () => {
                    await time.revert(snapshot)
                })
                it("cancel order 1",async () => {
                    await expect(
                        Context.p2PPool.connect(Context.signers.user2).closePosition(1, blockNumber[1])
                    ).to.emit(Context.p2PPool, 'ClosePosition').withArgs(positionHash[1])
                    expect(await Context.p2PPool.first()).eq('2')
                    expect(await Context.p2PPool.last()).eq('3')
                    const position2 = await Context.p2PPool["getPosition(bytes32)"](positionHash[2])
                    const position3 = await Context.p2PPool["getPosition(bytes32)"](positionHash[3])
                    comparePosition(position2, {
                        closed: false,
                        takerAmount: ethers.utils.parseUnits('0'),
                        makerAmount: ethers.utils.parseUnits('5'),
                        owner: Context.signers.user2.address,
                    })
                    expect(await Context.p2PPool.nextPosition(2)).eq(3)
                    expect(await Context.p2PPool.previousPosition(2)).eq(0)
                    comparePosition(position3, {
                        closed: false,
                        takerAmount: ethers.utils.parseUnits('0'),
                        makerAmount: ethers.utils.parseUnits('1'),
                        owner: Context.signers.user2.address,
                    })
                    expect(await Context.p2PPool.nextPosition(3)).eq(0)
                    expect(await Context.p2PPool.previousPosition(3)).eq(2)
                    expect(
                        await Context.p2PPool.getCapacity()
                    ).eq(ethers.utils.parseUnits('6'))
                })
                it("cancel order 2",async () => {
                    await expect(
                        Context.p2PPool.connect(Context.signers.user2).closePosition(2, blockNumber[2])
                    ).to.emit(Context.p2PPool, 'ClosePosition').withArgs(positionHash[2])
                    expect(await Context.p2PPool.first()).eq('1')
                    expect(await Context.p2PPool.last()).eq('3')
                    const position1 = await Context.p2PPool["getPosition(bytes32)"](positionHash[1])
                    const position3 = await Context.p2PPool["getPosition(bytes32)"](positionHash[3])
                    comparePosition(position1, {
                        closed: false,
                        takerAmount: ethers.utils.parseUnits('0'),
                        makerAmount: ethers.utils.parseUnits('3'),
                        owner: Context.signers.user2.address,
                    })
                    expect(await Context.p2PPool.nextPosition(1)).eq(3)
                    expect(await Context.p2PPool.previousPosition(1)).eq(0)
                    comparePosition(position3, {
                        closed: false,
                        takerAmount: ethers.utils.parseUnits('0'),
                        makerAmount: ethers.utils.parseUnits('1'),
                        owner: Context.signers.user2.address,
                    })
                    expect(await Context.p2PPool.nextPosition(3)).eq(0)
                    expect(await Context.p2PPool.previousPosition(3)).eq(1)
                    expect(
                        await Context.p2PPool.getCapacity()
                    ).eq(ethers.utils.parseUnits('4'))
                })
                it("cancel order 3",async () => {
                    await expect(
                        Context.p2PPool.connect(Context.signers.user2).closePosition(3, blockNumber[3])
                    ).to.emit(Context.p2PPool, 'ClosePosition').withArgs(positionHash[3])
                    expect(await Context.p2PPool.first(), "first").eq('1')
                    expect(await Context.p2PPool.last() , "last").eq('2')
                    const position1 = await Context.p2PPool["getPosition(bytes32)"](positionHash[1])
                    const position2 = await Context.p2PPool["getPosition(bytes32)"](positionHash[2])
                    comparePosition(position1, {
                        closed: false,
                        takerAmount: ethers.utils.parseUnits('0'),
                        makerAmount: ethers.utils.parseUnits('3'),
                        owner: Context.signers.user2.address,
                    })
                    expect(await Context.p2PPool.nextPosition(1)).eq(2)
                    expect(await Context.p2PPool.previousPosition(1)).eq(0)
                    comparePosition(position2, {
                        closed: false,
                        takerAmount: ethers.utils.parseUnits('0'),
                        makerAmount: ethers.utils.parseUnits('5'),
                        owner: Context.signers.user2.address,
                    })
                    expect(await Context.p2PPool.nextPosition(2)).eq(0)
                    expect(await Context.p2PPool.previousPosition(2)).eq(1)
                    expect(
                        await Context.p2PPool.getCapacity()
                    ).eq(ethers.utils.parseUnits('8'))
                })
            })
        })
        describe('Execute positions', () => {
            before(async () => {
                await Context.mechanics.connect(Context.signers.admin).mint(
                    Context.tokenARS.address,
                    Context.signers.user.address,
                    ethers.utils.parseUnits('1000')
                )
            })
            describe('First position', () => {
                it('Swap using P2P', async () => {
                    Decimal.set({precision: 18})
                    const factor = (new Decimal('10')).pow(18)
                    const amount = new Decimal('1')
                    let price:BigNumber|Decimal = await Context.oracle.getAnswer(1)
                    price = new Decimal(price.toString()).div(10 ** 6)
                    let fromAmount = amount.times(price)
                    let commission = fromAmount.times('0.02')

                    console.log({
                        fromAmount: fromAmount.toString(),
                        commission: commission.toString(),
                    })
                    let tx
                    const _snapshot = await time.snapshot()
                    await expect(() =>
                        tx = Context.swap.connect(Context.signers.user)
                            .swap(Context.pairHash, ethers.utils.parseUnits('1'), 1, ethers.constants.HashZero)
                    ).to.changeTokenBalances(Context.tokenARS,
                        [
                            Context.signers.user,
                            Context.signers.vault,
                            Context.signers.vault2,
                            Context.p2PPool
                        ], [
                            (fromAmount.plus(commission)).times(factor).negated().trunc().toString(),
                            (commission).times(factor).trunc().toString(),
                            (commission).times(factor).trunc().toString(),
                            (fromAmount.minus(commission)).times(factor).trunc().toString()
                        ]
                    )
                    await time.revert(_snapshot)
                    await expect(() =>
                        tx = Context.swap.connect(Context.signers.user)
                            .swap(Context.pairHash, ethers.utils.parseUnits('1'), 1, ethers.constants.HashZero)
                    ).to.changeTokenBalances(Context.tokenCLAD,
                        [
                            Context.signers.user,
                            Context.p2PPool,
                        ], [
                            (new Decimal('1')).times(factor).trunc().toString(),
                            (new Decimal('-1'  )).times(factor).trunc().toString(),
                        ]
                    )
                    await expect(tx).to.emit(Context.p2PPool, 'ExecutePosition').withArgs(
                        '1'
                    ).to.emit(Context.p2PPool, 'ExecutePositions').withArgs(
                        fromAmount.times(factor).trunc().toString(),
                        commission.times(factor).trunc().toString(),
                        '1'
                    )
                    expect(await Context.p2PPool.first()).eq(1)
                })
                it('Capacity', async ()=> {
                    expect(
                        await Context.p2PPool.getCapacity()
                    ).eq(ethers.utils.parseUnits('8'))
                    expect(
                        await Context.tokenCLAD.balanceOf(Context.p2PPool.address)
                    ).eq(ethers.utils.parseUnits('8'))
                    expect(
                        await Context.tokenARS.balanceOf(Context.p2PPool.address)
                    ).eq(ethers.utils.parseUnits('68.6'))
                })
                it('Claim partial filled position by third-part should be denied', async () => {
                    await expect(
                        Context.p2PPool.connect(Context.signers.thirdPart).closePosition(1, blockNumber[1])
                    ).to.revertedWith('Not delegable')
                })
                it('Claim partial filled position', async () => {
                    Decimal.set({precision: 18})
                    let _snapshot = await time.snapshot()
                    const factor = (new Decimal('10')).pow(18)
                    let tx

                    const data = Context.p2PPool.interface.encodeFunctionData("closePosition", [
                        '1', blockNumber[1]
                    ])
                    const network = await ethers.provider.getNetwork()
                    const gas = await ethers.provider.estimateGas({
                        from: Context.signers.user2.address,
                        to: Context.p2PPool.address,
                        data
                    })
                    const domain: EIP712Domain = {
                        chainId: network.chainId,
                        name: 'name',
                        version: 'version',
                        verifyingContract: Context.forwarder.address
                    }
                    const message: ForwardRequest = {
                        from: Context.signers.user2.address,
                        to: Context.p2PPool.address,
                        value: 0,
                        nonce: 0,
                        gas: gas.toNumber(),
                        data
                    }
                    const signature = await singMetatx(Context.signers.user2.address, domain, message)
                    await expect(() =>
                        tx = Context.forwarder.connect(Context.signers.service).execute(message,signature)
                    ).to.changeTokenBalances(Context.tokenARS,
                        [
                            Context.signers.user2,
                            Context.p2PPool
                        ], [
                            (new Decimal('68.6')).times(factor).trunc().toString(),
                            (new Decimal('-68.6'  )).times(factor).trunc().toString(),
                        ]
                    )
                    await time.revert(_snapshot)
                    _snapshot = await time.snapshot()
                    await expect(() =>
                        tx = Context.p2PPool.connect(Context.signers.user2).closePosition(1, blockNumber[1])
                    ).to.changeTokenBalances(Context.tokenCLAD,
                        [
                            Context.signers.user2,
                            Context.p2PPool
                        ], [
                            (new Decimal('2')).times(factor).trunc().toString(),
                            (new Decimal('-2'  )).times(factor).trunc().toString(),
                        ]
                    )
                    await expect(tx).to.emit(Context.p2PPool, 'ClosePosition').withArgs(
                        positionHash[1]
                    )
                    await time.revert(_snapshot)
                    expect(
                        await Context.p2PPool.getCapacity()
                    ).eq(ethers.utils.parseUnits('8'))
                })
            })
            describe('Cross position', () => {
                it('Execute', async () => {
                    Decimal.set({precision: 18})
                    const factor = (new Decimal('10')).pow(18)
                    const amount = new Decimal('8')
                    let price:BigNumber|Decimal = await Context.oracle.getAnswer(1)
                    price = new Decimal(price.toString()).div(10 ** 6)
                    let fromAmount = amount.times(price.toString())
                    let commission = fromAmount.times('0.02')

                    let tx
                    let _snapshot = await time.snapshot()
                    await expect(() =>
                        tx = Context.swap.connect(Context.signers.user)
                            .swap(Context.pairHash, amount.times(factor).trunc().toString(), 1, ethers.constants.HashZero)
                    ).to.changeTokenBalances(Context.tokenARS,
                        [
                            Context.signers.user,
                            Context.signers.vault,
                            Context.signers.vault2
                        ], [
                            fromAmount.plus(commission).neg().times(factor).trunc().toString(),
                            commission.times(factor).trunc().toString(),
                            commission.times(factor).trunc().toString(),
                        ]
                    )
                    await time.revert(_snapshot)
                    await expect(() =>
                        tx = Context.swap.connect(Context.signers.user)
                            .swap(Context.pairHash, amount.times(factor).trunc().toString(), 1, ethers.constants.HashZero)
                    ).to.changeTokenBalances(Context.tokenCLAD,
                        [
                            Context.signers.user,
                            Context.p2PPool,
                        ], [
                            (new Decimal('8')).times(factor).trunc().toString(),
                            (new Decimal('-8'  )).times(factor).trunc().toString(),
                        ]
                    )
                    await expect(tx).to.emit(Context.p2PPool, 'ExecutePosition').withArgs(
                        '1',
                        positionHash[1],
                        true
                    ).to.emit(Context.p2PPool, 'ExecutePositions').withArgs(
                        fromAmount.times(factor).trunc().toString(),
                        commission.times(factor).trunc().toString(),
                        '1'
                    )
                    expect(await Context.p2PPool.first()).eq(0)
                    expect(await Context.p2PPool.last()).eq(0)
                    expect(await Context.p2PPool.capacity()).eq(0)
                })
                it("Third part can claim an executed order", async () => {
                    await expect(Context.p2PPool.connect(Context.signers.thirdPart).closePosition(2, blockNumber[2]))
                        .to.emit(Context.p2PPool, 'ClosePosition').withArgs(positionHash[2])
                })
            })
            it('Swap using P2P', async () => {
                const factor = (new Decimal('10')).pow(18)
                let capacity = new Decimal((await Context.p2PPool.capacity()).toString())
                let price:BigNumber|Decimal = await Context.oracle.getAnswer(1)
                price = new Decimal(price.toString()).div(10 ** 6)
                let fromAmount = capacity.times(price)
                Context.swap.connect(Context.signers.user)
                    .swap(Context.pairHash, capacity.times(factor).trunc().toString(), 1, ethers.constants.HashZero)
                capacity = new Decimal((await Context.p2PPool.capacity()).toString())
                console.log({capacity})
            })
            xit('Swap using P2P', async () => {
                await Context.swap.connect(Context.signers.user).swap(
                    Context.pairHash,
                    ethers.utils.parseUnits('8'),
                    1,
                    ethers.constants.HashZero
                )
            })
        })
    })
})