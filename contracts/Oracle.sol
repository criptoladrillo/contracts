// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "@openzeppelin/contracts/access/AccessControl.sol";
import "./IOracle.sol";

contract Oracle is AccessControl, IOracle {

    mapping (uint80 => int256) private _answer;
    mapping (uint80 => uint256) private _timestamp;
    uint80 private _latestRoundId;

    event AnswerUpdated(
        int256 indexed current,
        uint80 indexed roundId,
        uint256 updatedAt
    );

    constructor (address admin, int256 answer) {
        _setupRole(DEFAULT_ADMIN_ROLE, admin);
        _updateAnswer(answer);
    }

    modifier onlyAdmin(){
        require(hasRole(DEFAULT_ADMIN_ROLE, _msgSender()), "onlyAdmin");
        _;
    }

    function latestAnswer() external view returns (int256) {
        return _answer[_latestRoundId];
    }

    function latestTimestamp() external view returns (uint256) {
        return _timestamp[_latestRoundId];
    }

    function latestRound() public view returns (uint80) {
        return _latestRoundId;
    }

    function getAnswer(uint80 roundId) public view returns (int256) {
        return _answer[roundId];
    }

    function getTimestamp(uint80 roundId) external view returns (uint256) {
        return _timestamp[roundId];
    }

    function decimals() public pure returns (uint8) {
        return 6;
    }

    function latestRoundData()
    external
    view
    returns (
        uint80 roundId,
        int256 answer,
        uint256 startedAt,
        uint256 updatedAt,
        uint80 answeredInRound
    ) {
        roundId = latestRound();
        answer =  getAnswer(roundId);
    }

    function _updateAnswer(int256 answer) private {
            _latestRoundId = _latestRoundId + 1;
            _answer[_latestRoundId] = answer;
            _timestamp[_latestRoundId] = block.timestamp;
            emit AnswerUpdated(answer, _latestRoundId, block.timestamp);
        }

    function updateAnswer(int256 answer) public onlyAdmin {
        _updateAnswer(answer);
    }

    function getLastRound() external override view returns (uint80 roundId, int256 answer, uint8 decimals_){
        roundId = latestRound();
        answer =  getAnswer(roundId);
        decimals_ = decimals();
    }
}