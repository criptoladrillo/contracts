// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "../IP2PPool.sol";

contract P2PPoolMock is IP2PPool {
    function getCapacity() external override view returns (uint256) {
        return 0;
    }

    function executePositions(address fromAccount, uint256 amount, uint80 oracleRoundId) external override {
        return;
    }

    function openPosition(uint256 putAmount) external override returns (uint256 positionId) {
        return 0;
    }

    function closePosition(uint256 positionId, uint256 blockNumber) external override {}
}