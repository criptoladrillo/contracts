import { task } from "hardhat/config";
import "@nomiclabs/hardhat-waffle";
import '@openzeppelin/hardhat-upgrades';
import "@nomiclabs/hardhat-ethers";
import '@typechain/hardhat'
import 'hardhat-tracer'
import "solidity-coverage"
import "@nomiclabs/hardhat-etherscan";
import "hardhat-deploy"
import "./extensions/time"


// This is a sample Hardhat task. To learn how to create your own go to
// https://hardhat.org/guides/create-task.html
task("accounts", "Prints the list of accounts", async (args, hre) => {
  const accounts = await hre.ethers.getSigners();

  for (const account of accounts) {
    console.log(await account.address);
  }
});

// You need to export an object to set up your config
// Go to https://hardhat.org/config/ to learn more

export default {
  etherscan: {
    apiKey: process.env.ETHERSCAN_TOKEN,
    customChains: [],
  },
  solidity: {
    version: "0.8.9",
    settings: {
      optimizer: {
        enabled: true,
        runs: 200
      }
    },
  },
  typechain: {
    target: 'ethers-v5'
  },
  networks:{
    rinkeby: {
      url: `https://rinkeby.infura.io/v3/a946314d87a3490bac0bc774a82c0c4c`
    },
    goerli: {
      url: `https://goerli.infura.io/v3/a946314d87a3490bac0bc774a82c0c4c`
    },
    xdai: {
      url: 'https://stake.getblock.io/mainnet/?api_key=f92b97e6-8e32-4671-9258-67fddce96f0c',
      //url: 'http://bicha.lab.riaquest.com:8545'
    },
    arbitrum: {
      url: 'https://rinkeby.arbitrum.io/rpc',
    },
    hardhat: {
      initialBaseFeePerGas: 0
    }
  }
};