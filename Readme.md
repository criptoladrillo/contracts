> Dev - xDAI Mainnet


|Account  |Type |Address                                     |
|---------|-----|--------------------------------------------|
| Admin   | MS  | 0x49B41F195d11906FE872a31f37A4aA93b6662d31 |
| Vault   | MS  | 0x8b4d7Ed2255281B595141912f37B9C535206B0ED |
| Service | MS  | 0xAd1ec246a406A95162aB733774cD5590766A4812 |
| Deployer | EOA| 0xF94d77AF2c48e06F189B3839bba9592D3699133A |


| Contract | Address |
|----------|---------|
|Mechanics |0x2f975Bb9003BDF760c13f64659bF87a71D9F4805|
|Forwarder |0xe15eA2cA0a76a61ACFCc2BF9ED96b3dd78DfF9a4|
|Token ARSD|0x6Ec582B1aa36A1d72cfa66dd83Be6cc99E8Cd8FF|
|Token CLAD|0xAFDD2A6281F84d9c33F79208ed99eBc9dB454c92|
|Oracle    |0x868970a99F9797b558EC09738c7E938831531938|
|Swap      |0x5f988AA2e2c24E88FE6655984E549E91d29FCd58|

## Verify 
Mechanic
```bash
npx hardhat verify --network rinkeby 0x2f975Bb9003BDF760c13f64659bF87a71D9F4805 0x49B41F195d11906FE872a31f37A4aA93b6662d31
```
Forwarder
```bash
npx hardhat verify --network rinkeby 0xe15eA2cA0a76a61ACFCc2BF9ED96b3dd78DfF9a4 0x49B41F195d11906FE872a31f37A4aA93b6662d31 0xAd1ec246a406A95162aB733774cD5590766A4812 "Criptoladrillo" "2"
```
Token ARSD
```bash
npx hardhat verify --network rinkeby 0x6Ec582B1aa36A1d72cfa66dd83Be6cc99E8Cd8FF 0x49B41F195d11906FE872a31f37A4aA93b6662d31 0x2f975Bb9003BDF760c13f64659bF87a71D9F4805 0xe15eA2cA0a76a61ACFCc2BF9ED96b3dd78DfF9a4 "Moneda Argentina Digital" "ARSD"
```
Token CLAD
```bash
npx hardhat verify --network rinkeby 0xAFDD2A6281F84d9c33F79208ed99eBc9dB454c92 0x49B41F195d11906FE872a31f37A4aA93b6662d31 0x2f975Bb9003BDF760c13f64659bF87a71D9F4805 0xe15eA2cA0a76a61ACFCc2BF9ED96b3dd78DfF9a4 "Criptoladrillo" "CLAD"
```
Oracle
```bash
npx hardhat verify --network rinkeby 0x868970a99F9797b558EC09738c7E938831531938 0x49B41F195d11906FE872a31f37A4aA93b6662d31 56000000
```
Swap
```bash
npx hardhat verify --network rinkeby 0x5f988AA2e2c24E88FE6655984E549E91d29FCd58 0x49B41F195d11906FE872a31f37A4aA93b6662d31 0xe15eA2cA0a76a61ACFCc2BF9ED96b3dd78DfF9a4 0x49B41F195d11906FE872a31f37A4aA93b6662d31
```

  
  
## Finalize setup
* Mint ARSD to `Deployer`  
  1.000.000  
  https://rinkeby.etherscan.io/tx/0x51b3ab4298c03cba9dfc1dcce51b23f95c985d64fc41cbc41d97e11918228ccc
* Mint CLAD to `Vault`
  1.000.000  
  https://rinkeby.etherscan.io/tx/0x90b6a9193606b2dcf38d413ec93d5756140fe9af4990467dda16490aef166096
* Approve `Vault` CLAD to Swap
  500
  https://rinkeby.etherscan.io/tx/0x727ace9fb2b9d92c752db10bf0c09c89958e3c39e93ec53628e1c3b634dfd7f2
* Add pair ARSD/CLAD on Swap
  Commission 2%
  https://rinkeby.etherscan.io/tx/0x064af9a866ebb38f80e503eac4d675d8ac65d6c9a232c672619da520083ffaf1
* Set swap as core contract on mechanics
  https://rinkeby.etherscan.io/tx/0x8971971b432bee9b4f251b3f3e675b422bd67b98b6a0853723cae1989cd5ee02

* Swap de pruebas
  https://rinkeby.etherscan.io/tx/0xc905bf2bf206e1f3c57d02d1fd7b75dcb7080233156e746066f0e49770953f4d#eventlog

Mint a una cuenta pelada
https://blockscout.com/xdai/mainnet/tx/0x4c28e9957f9bc8adaeaa946a878b644c49fe8d2520a6076c4c566086b66a95cf

Set del oraculo
https://blockscout.com/xdai/mainnet/tx/0x09e351d74c85809ecfa12631c082ae860b1bc463fd7997fe465c1a27bc3b2587

Recuperar CLAD de la MultiSig del warehouse 
https://blockscout.com/xdai/mainnet/tx/0x635fb117d5044c27bf5b38d02a12edbe29d093b507acd0b83730c402c5fcc5bf
