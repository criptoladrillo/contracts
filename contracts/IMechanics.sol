// contracts/DtecT.sol
// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

interface IMechanics {
    /** Policy **/
    function isTransferable(address token, address from, address to, uint256 amount) external view returns (bool);
    /** Operations **/
    function isOperable(address token, address operator, address from, address to, uint256 amount) external view returns (bool);
    /** Finance **/
    function mint(address token, address account, uint256 amount) external;
    function burn(address token, address account, uint256 amount) external;
}