import { ethers } from 'hardhat'
import { Oracle } from "../typechain";
import {SignerWithAddress} from "@nomiclabs/hardhat-ethers/dist/src/signer-with-address";
import {shouldFailWith} from "./utils";
import {expect} from "chai";
import exp from "constants";

interface TestContext {
    signers: {
        admin: SignerWithAddress,
        thirdparty: SignerWithAddress
    }
    oracle: Oracle
}

describe('Oracle', function() {

    let Context: TestContext

    before(async function () {
        const [admin, thirdparty] = await ethers.getSigners()

        const Oracle = await ethers.getContractFactory("Oracle")
        const oracle = await Oracle.deploy(admin.address, '20') as Oracle;

        const signers = {
            admin,
            thirdparty,
        }
        Context = {
            oracle,
            signers
        }
    })

    describe('Update', () => {
        it('latestAnswer', async () => {
            const answer = await Context.oracle.latestAnswer()
            await expect(answer.toString()).eq('20')
        });
        it('update by thirdparty should revert', async () => {
            await expect(
                Context.oracle.connect(Context.signers.thirdparty).updateAnswer('21')
            ).to.revertedWith('onlyAdmin')
        });
        it('update by admin should succeed', async () => {
            const tx = await Context.oracle.connect(Context.signers.admin).updateAnswer('21')
            const answer = await Context.oracle.latestAnswer()
            expect(answer.toString()).eq('21')
        });
    })

})