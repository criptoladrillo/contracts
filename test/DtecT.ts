import { expect } from 'chai'
import { ethers } from 'hardhat'
import { DtecT, Mechanics, Forwarder } from "../typechain";
import { SignerWithAddress } from "@nomiclabs/hardhat-ethers/dist/src/signer-with-address";
import { shouldFailWith } from "./utils";

interface TestContext {
    signers: {
        [id: string]: SignerWithAddress
    }
    token: DtecT
    mechanics: Mechanics
}

describe('DtecA', function() {

    let Context: TestContext

    before(async function () {
        const [admin, user1, user2, operator, thirdparty] = await ethers.getSigners()

        const Forwarder = await ethers.getContractFactory("Forwarder")
        const forwarder = await Forwarder.deploy(admin.address,admin.address,'name','version') as Forwarder;

        const Mechanics = await ethers.getContractFactory("Mechanics")
        const mechanics = await Mechanics.deploy(admin.address) as Mechanics;

        const Token = await ethers.getContractFactory("DtecT")
        const token = await Token.deploy(admin.address,mechanics.address, forwarder.address, "A", "A") as DtecT;

        await mechanics.connect(admin).mint(token.address,user1.address,ethers.utils.parseUnits('10'))

        Context = {
            token,
            mechanics,
            signers: {
                admin,
                user1,
                user2,
                operator,
                thirdparty,
            }
        }
    })


    describe("Transfers", ()=>{
        describe("blacklist", () => {
            it('Blacklisted address should not transfer', async () => {
                const {token, mechanics, signers: { admin, user1, user2 }} = Context
                const blacklistTx = await mechanics.connect(admin).setBlacklistForAccount(user1.address, true)
                const blacklistReceipt = await blacklistTx.wait()
                expect(blacklistReceipt.status).eq(1)

                await expect(
                    token.connect(user1).transfer(user2.address, ethers.utils.parseUnits('1'))
                ).to.revertedWith('No transferable')
            })
            it('Not blaclisted address should transfer', async () => {
                const {token, mechanics, signers: { admin, user1, user2 }} = Context
                const blacklistTx = await mechanics.connect(admin).setBlacklistForAccount(user1.address, false)
                const blacklistReceipt = await blacklistTx.wait()
                expect(blacklistReceipt.status).eq(1)

                const tx = await token.connect(user1).transfer(user2.address, ethers.utils.parseUnits('1'))
                const receipt = await tx.wait()
                expect(receipt.status).eq(1)
            })
        })
        describe('coreContract', ()=>{
            it('third-party should not operate', async ()=>{
                const {token, mechanics, signers: { admin, user1, user2, operator }} = Context
                await expect(token.connect(operator).operateTransferFrom(
                    user1.address,
                    user2.address,
                    ethers.utils.parseUnits('1')
                )).to.revertedWith('No operable')
            })
            it('operator should operate', async ()=>{
                const {token, mechanics, signers: { admin, user1, user2, operator }} = Context
                const txCC = await await mechanics.connect(admin).setCoreContract(operator.address, true)
                const ccReceipt = await txCC.wait(1)
                expect(ccReceipt.status).eq(1)
                const tx = await token.connect(operator).operateTransferFrom(
                    user1.address,
                    user2.address,
                    ethers.utils.parseUnits('1')
                )
                const receipt = await tx.wait(1)
                expect(receipt.status).eq(1)
            })
        })
    })
})