import { expect } from 'chai'
import { ethers } from 'hardhat'
import * as Contracts from "../typechain";
import {DtecT} from "../typechain";
import {SignerWithAddress} from "@nomiclabs/hardhat-ethers/dist/src/signer-with-address";
import {EIP712Domain, ForwardRequest, singMetatx} from "./utils";
import {BigNumber} from "ethers";

describe('BankBridge', function() {
    let forwarder:Contracts.Forwarder
    let mechanics:Contracts.Mechanics
    let token:Contracts.DtecT
    let bankbridge:Contracts.BankBridge

    let deployer: SignerWithAddress,
        admin: SignerWithAddress,
        tapir: SignerWithAddress,
        carpincho: SignerWithAddress,
        user: SignerWithAddress,
        thirdparty: SignerWithAddress

    const TAPIR_ROLE = ethers.utils.solidityKeccak256(['string'], ['TAPIR_ROLE'])

    const firstOperationHash = ethers.utils.solidityKeccak256(['string'],["op1"])
    const firstOperationAmount = ethers.utils.parseUnits('1')

    const batchDeposits = {
        operationsHash: [
            ethers.utils.solidityKeccak256(['string'],["BatchDeposit1"]),
            ethers.utils.solidityKeccak256(['string'],["BatchDeposit2"])
        ],
        destinations: [] as string[],
        amounts: [
            ethers.utils.parseUnits('0.1', 'ether'),
            ethers.utils.parseUnits('0.2', 'ether')
        ]
    }

    before(async () => {
        [deployer, admin, carpincho, tapir, user, thirdparty] = await ethers.getSigners()

        batchDeposits.destinations = [
            user.address,
            user.address
        ]

        forwarder = await new Contracts.Forwarder__factory(deployer).deploy(admin.address, carpincho.address,'name','version');

        mechanics = await new Contracts.Mechanics__factory(deployer).deploy(admin.address);

        token = await new Contracts.DtecT__factory(deployer).deploy(admin.address,mechanics.address, forwarder.address, "TEST", "TST");

        bankbridge = await new Contracts.BankBridge__factory(deployer).deploy(admin.address, ethers.constants.AddressZero, tapir.address, [ethers.constants.HashZero])

        await mechanics.connect(admin).mint(token.address,bankbridge.address,ethers.utils.parseUnits('10'))
        await mechanics.connect(admin).setCoreContract(bankbridge.address, true)
    })
    describe('UpdateForwarder', () => {
        it('should revert from thirdparty', async () => {
            await expect(
                bankbridge.connect(thirdparty).updateForwarder(forwarder.address)
            ).to.be.revertedWith('Ownable: caller is not the owner')
        })
        it('should succeed from owner', async () => {
            await bankbridge.connect(admin).updateForwarder(forwarder.address)
        })
    })
    describe('Deposit', () => {
        describe('Role Tapir', () => {
            describe('SingleDeposit', () => {
                it('should revert from thirdparty', async () => {
                    await expect(
                        bankbridge.connect(thirdparty).deposit(
                            firstOperationHash,
                            user.address,
                            firstOperationAmount,
                            token.address
                        )
                    ).to.be.revertedWith(`AccessControl: account ${thirdparty.address.toLocaleLowerCase()} is missing role ${TAPIR_ROLE}`)
                })
                it('should succeed from tapir', async () => {
                    await expect(
                        bankbridge.connect(tapir).deposit(
                            firstOperationHash,
                            user.address,
                            firstOperationAmount,
                            token.address
                        )
                    ).to.emit(token, 'Transfer')
                    .withArgs(bankbridge.address, user.address, firstOperationAmount);
                })
            })
        })
        describe('BatchDeposit', () => {
            it('should revert from thirdparty', async () => {
                await expect(
                    bankbridge.connect(thirdparty).batchDeposit(
                        batchDeposits.operationsHash,
                        batchDeposits.destinations,
                        batchDeposits.amounts,
                        token.address
                    )
                ).to.be.revertedWith(`AccessControl: account ${thirdparty.address.toLocaleLowerCase()} is missing role ${TAPIR_ROLE}`)
            })
            it('should succeed from tapir', async () => {
                await expect(
                    bankbridge.connect(tapir).batchDeposit(
                        batchDeposits.operationsHash,
                        batchDeposits.destinations,
                        batchDeposits.amounts,
                        token.address
                    )
                ).to.emit(token, 'Transfer')
                    .withArgs(bankbridge.address, user.address, ethers.utils.parseUnits('0.1', 'ether'))
                .to.emit(bankbridge, 'Deposit')
                    .withArgs(
                        ethers.utils.solidityKeccak256(['string'], ["BatchDeposit1"]),
                        user.address,
                        ethers.utils.parseUnits('0.1', 'ether'),
                        token.address
                    )
                .to.emit(token, 'Transfer')
                    .withArgs(bankbridge.address, user.address, ethers.utils.parseUnits('0.2', 'ether'))
                .to.emit(bankbridge, 'Deposit')
                    .withArgs(
                        ethers.utils.solidityKeccak256(['string'], ["BatchDeposit2"]),
                        user.address,
                        ethers.utils.parseUnits('0.2', 'ether'),
                        token.address
                    )
            })
        })
        describe('Repeat', () => {
            it('should omit repeat deposit', async () => {
                const tx = await bankbridge.connect(tapir).deposit(
                    firstOperationHash,
                    user.address,
                    firstOperationAmount,
                    token.address
                )
                const receipt = await tx.wait()
                expect(receipt.logs.length).to.be.eq(0)
            })
            it('should omit repeat batchDeposit', async () => {
                const tx = await bankbridge.connect(tapir).batchDeposit(
                    batchDeposits.operationsHash,
                    batchDeposits.destinations,
                    batchDeposits.amounts,
                    token.address
                )
                const receipt = await tx.wait()
                expect(receipt.logs.length).to.be.eq(0)
            });
        })
    })
    describe('Withdrawal', () => {
        let amount: BigNumber,
            data: string,
            gas: BigNumber,
            domain: EIP712Domain
        before(async () => {
            amount = ethers.utils.parseUnits('0.111', 'ether')
            data = bankbridge.interface.encodeFunctionData("withdrawal", [
                ethers.utils.solidityKeccak256(['string'],["Withdrawal1"]),
                amount,
                token.address
            ])
            const network = await ethers.provider.getNetwork()
            gas = await ethers.provider.estimateGas({
                from: user.address,
                to: bankbridge.address,
                data
            })
            domain = {
                chainId: network.chainId,
                name: 'name',
                version: 'version',
                verifyingContract: forwarder.address
            }
        })
        it('should succeed using forwarder', async () => {
            const message: ForwardRequest = {
                from: user.address,
                to: bankbridge.address,
                value: 0,
                nonce: 0,
                gas: gas.toNumber(),
                data
            }
            const signature = await singMetatx(user.address, domain, message)
            await expect(
                forwarder.connect(carpincho).execute(message, signature)
            ).to.emit(token, 'Transfer')
                .withArgs(user.address, bankbridge.address, amount)
        });
        it('should revert duplicated', async () => {
            const message: ForwardRequest = {
                from: user.address,
                to: bankbridge.address,
                value: 0,
                nonce: 1,
                gas: gas.toNumber(),
                data
            }
            const signature = await singMetatx(user.address, domain, message)
            const tx = await forwarder.connect(carpincho).execute(message, signature)
            const receipt = await tx.wait()
            expect(receipt.logs.length).to.be.eq(0)
        });
    })
})