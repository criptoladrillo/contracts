// contracts/DtecT.sol
// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/metatx/ERC2771Context.sol";
import "./IMechanics.sol";
import "./IDtecT.sol";

contract DtecT is ERC20, Ownable, ERC2771Context, IDtecT {

    IMechanics private _mechanics;
    address internal _forwarder;

    modifier onlyMechanics() {
        require(msg.sender == address(_mechanics), "onlyMechanics");
        _;
    }

    event MechanicsUpdated(IMechanics indexed _address);

    constructor(address owner, IMechanics mechanics_, address forwarder, string memory name, string memory symbol) ERC20(name, symbol) ERC2771Context(forwarder) Ownable() {
        transferOwnership(owner);
        _mechanics = mechanics_;
        _forwarder = forwarder;
    }

    function isTrustedForwarder(address forwarder) public view override returns (bool) {
        return forwarder == _forwarder;
    }

    function updateForwarder(address forwarder) external onlyOwner {
        _forwarder = forwarder;
    }

    function mechanics() external view returns (IMechanics) {
        return _mechanics;
    }

    function updateMechanics(IMechanics mechanics_) external onlyOwner {
        _mechanics = mechanics_;
        emit MechanicsUpdated(_mechanics);
    }

    function mint(address account, uint256 amount) onlyMechanics external override {
        _mint(account, amount);
    }

    function burn(address account, uint256 amount) onlyMechanics external override {
        _burn(account, amount);
    }

    function _beforeTokenTransfer(address from, address to, uint256 amount) internal override {
        require(_mechanics.isTransferable(address(this), from, to, amount), "No transferable");
    }

    function operateTransferFrom(address from, address to, uint256 amount ) public override {
        require(_mechanics.isOperable(address(this), _msgSender(), from, to, amount), "No operable");
        _transfer(from, to, amount);
    }

    function _msgSender() internal view virtual override(Context, ERC2771Context) returns (address sender) {
        return ERC2771Context._msgSender();
    }

    function _msgData() internal view virtual override(Context, ERC2771Context) returns (bytes calldata) {
        return ERC2771Context._msgData();
    }
}