import {expect, use} from 'chai';
import chaiString from 'chai-string'
import {ethers} from "hardhat";
import {BigNumber} from "ethers";
use(chaiString)

export async function shouldFailWith(tx: Promise<any>, error: string){
    try {
        await tx
        expect(false).eq(true, "Should fail")
    } catch (e) {
        // @ts-ignore
        const message: string = (e.message || e) as unknown as string
        console.log(`Message: ${message}`)
        expect(message).endWith(error)
    }
}

export interface EIP712Domain {
    name:string
    version: string
    chainId: number
    verifyingContract: string
}

export interface ForwardRequest {
    from: string
    to: string
    value: number
    gas: number
    nonce: number
    data: string
}

export async function singMetatx(
    fromAddress: string,
    domain: EIP712Domain,
    message: ForwardRequest,
){
    const compose = {
        types: {
            EIP712Domain: [
                { name: "name", type: "string" },
                { name: "version", type: "string" },
                { name: "chainId", type: "uint256" },
                { name: "verifyingContract", type: "address" },
            ],
            ForwardRequest: [
                { name: 'from', type: 'address' },
                { name: 'to', type: 'address' },
                { name: 'value', type: 'uint256' },
                { name: 'gas', type: 'uint256' },
                { name: 'nonce', type: 'uint256' },
                { name: 'data', type: 'bytes' },
            ]
        },
        domain,
        primaryType: "ForwardRequest",
        message
    }
    return ethers.provider.send('eth_signTypedData_v4',[fromAddress, compose])
}

export function comparePosition(p1: any, p2: any){
    ['closed', 'nextPosition', 'previousPosition', 'owner', 'fromAmount', 'toAmount'].forEach( attr => {
        expect(p1[attr],attr).eq(p2[attr])
    })
}