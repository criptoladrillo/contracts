// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/metatx/ERC2771ContextUpgradeable.sol";
import "./IOracle.sol";
import "./IDtecT.sol";
import "./IP2PPool.sol";

contract SwapWithOracles is OwnableUpgradeable, ERC2771ContextUpgradeable {
    address  private  _platform;

    mapping (bytes32 => IDtecT)   private _fromToken;
    mapping (bytes32 => IDtecT)   private _toToken;
    mapping (bytes32 => IOracle)  private _oracle;
    mapping (bytes32 => address)  private _provider;
    mapping (bytes32 => IP2PPool) private _p2p;
    mapping (bytes32 => uint256)  private _commission;
    mapping (bytes32 => bool)     private _exist;

    uint256 public constant COMMISSION_EXPONENT = 1000000;

    struct Pair {
        IDtecT   fromToken;
        IDtecT   toToken;
        IOracle  oracle;
        address  provider;
        IP2PPool p2p;
        uint256  commission;
    }

    event Swap (IOracle indexed oracleAddress, uint256 indexed oracleRoundId, bytes32 indexed operationId);

    event PairCreated(IDtecT  indexed fromToken, IDtecT indexed toToken, bytes32 indexed pairHash);
    event PairUpdate (bytes32 indexed pairHash);
    event PairRemove (bytes32 indexed pairHash);

    modifier pairHashExist(bytes32 pairHash){
        require(_exist[pairHash], "Undefined");
        _;
    }

    modifier pairHashNotExist(bytes32 pairHash){
        require(!_exist[pairHash], "Defined");
        _;
    }

    /// @custom:oz-upgrades-unsafe-allow constructor
    constructor(address forwarder_) ERC2771ContextUpgradeable(forwarder_) {
        _disableInitializers();
    }

    function initialize(address admin, address platform_) public initializer {
        __Ownable_init();
        transferOwnership(admin);
        _platform  = platform_;
    }

    function computePairHash(IDtecT fromToken, IDtecT toToken) public pure returns (bytes32) {
        return keccak256(abi.encodePacked(fromToken, toToken));
    }

    function addPair(Pair memory pair) external onlyOwner {
        bytes32 pairHash = computePairHash(pair.fromToken, pair.toToken);
        require(!_exist[pairHash], "Defined");

        _fromToken [pairHash] = pair.fromToken;
        _toToken   [pairHash] = pair.toToken;
        _oracle    [pairHash] = pair.oracle;
        _provider  [pairHash] = pair.provider;
        _p2p       [pairHash] = pair.p2p;
        _commission[pairHash] = pair.commission;
        _exist     [pairHash] = true;

        emit PairCreated(pair.fromToken, pair.toToken, pairHash);
    }

    function updatePair(bytes32 pairHash, IOracle oracle, address provider, IP2PPool p2p, uint256 commission) external onlyOwner pairHashExist(pairHash) {
        _oracle    [pairHash] = oracle;
        _provider  [pairHash] = provider;
        _p2p       [pairHash] = p2p;
        _commission[pairHash] = commission;

        emit PairUpdate(pairHash);
    }

    function removePair(bytes32 pairHash) external onlyOwner pairHashExist(pairHash) {
        _exist     [pairHash] = false;

        emit PairRemove(pairHash);
    }

    function getPair(IDtecT fromToken, IDtecT toToken) public view returns (Pair memory) {
        bytes32 pairHash = computePairHash(fromToken, toToken);
        return Pair(
            _fromToken [pairHash],
            _toToken   [pairHash],
            _oracle    [pairHash],
            _provider  [pairHash],
            _p2p       [pairHash],
            _commission[pairHash]
        );
    }

    function getPair(bytes32 pairHash) public view returns (Pair memory) {
        return Pair(
            _fromToken [pairHash],
            _toToken   [pairHash],
            _oracle    [pairHash],
            _provider  [pairHash],
            _p2p       [pairHash],
            _commission[pairHash]
        );
    }

    function swap(bytes32 pairHash, uint256 toAmount, uint80 oracleRoundId, bytes32 operationId) external pairHashExist(pairHash) {
        Pair memory pair = getPair(pairHash);
        (uint80 roundID, int answer, uint8 decimals) = pair.oracle.getLastRound();
        require(oracleRoundId == roundID, "Outdated round");

        uint256 fromAmount = (toAmount * uint256(answer)) / 10 ** decimals;
        uint256 commission = (fromAmount * pair.commission) / COMMISSION_EXPONENT;
        uint256 toAmountFromPrimary = toAmount;

        uint256 p2pCapacity = pair.p2p.getCapacity();

        if (p2pCapacity > 0) {
            if (p2pCapacity <= toAmountFromPrimary) {
                pair.p2p.executePositions(_msgSender(), p2pCapacity, oracleRoundId);
                toAmountFromPrimary = toAmountFromPrimary - p2pCapacity;
            } else {
                pair.p2p.executePositions(_msgSender(), toAmountFromPrimary, oracleRoundId);
                toAmountFromPrimary = 0;
            }
        }
        if (toAmountFromPrimary > 0) {
            pair.fromToken.operateTransferFrom(_msgSender(), pair.provider, (toAmountFromPrimary * uint256(answer)) / 10 ** decimals);
            pair.toToken.transferFrom(pair.provider, _msgSender(), toAmountFromPrimary);
        }

        pair.fromToken.operateTransferFrom(_msgSender(), _platform, commission);

        emit Swap(pair.oracle, oracleRoundId, operationId);
    }

    function _msgSender() internal view virtual override(ContextUpgradeable, ERC2771ContextUpgradeable) returns (address) {
        return ERC2771ContextUpgradeable._msgSender();
    }

    function _msgData() internal view virtual override(ContextUpgradeable, ERC2771ContextUpgradeable) returns (bytes calldata) {
        return ERC2771ContextUpgradeable._msgData();
    }

    function platform() external view returns(address) {
        return _platform;
    }

    function setPlatform(address platform_) external onlyOwner {
        _platform = platform_;
    }
}